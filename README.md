# TP5: Créer et résoudre un conflit

## 1- Cloner un même dépôt git distant dans deux répertoires A et B
![Q1](Screenshots/TP5 Q1.png)

## 2- Dans le répertoire A, créer un fichier MonConflit.txt avec 3 lignes de texte:
![Q2](Screenshots/TP5 Q2.png)

## 3- Propagez cette modifications dans le dépôt distant:
![Q3](Screenshots/TP5 Q3.png)

## 4- Allez dans le répertoire B et synchronisez le dépôt:
![Q4](Screenshots/TP5 Q4.png)

## 5- Modifiez la 2ième ligne du fichier MonConflit.txt:
![Q5](Screenshots/TP5 Q5.png)

## 6- Propagez cette modifications dans le dépôt distant:
![Q6](Screenshots/TP5 Q6.png)

## 7- Revenez dans A et modifiez différemment la deuxième ligne du fichier:
![Q7](Screenshots/TP5 Q7.png)

## 8- Tentez de propager vos modifications sur le dépôt distant:
![Q8](Screenshots/TP5 Q8.png)

## 9- Synchronisez votre dépôt avec le dépôt distant:
![Q9](Screenshots/TP5 Q9.png)

## 10- Résoudre le conflit en éditant le fichier MonConflit.txt; ajouter ce fichier dans l’index pour valider la correction; commiter le résultat pour terminer la fusion; propagez cette version:
![Q10](Screenshots/TP5 Q10&11&12.png)